require('dotenv').config();


const express = require('express');// Nos traemos el framework. Similar a include
const isNumeric = require('isnumeric');
const app = express(); // inicializamos el framework
const io =require('./io');//Exportamos nuestros propios modulos. No se escribe la extension
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');


var enableCORS = function(req, res, next) {

 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();

}


app.use(express.json());
app.use(enableCORS);


const port = process.env.PORT || 3000; // Coje la variable de entorno y si no exite coje el puerto 3000
app.listen(port);
console.log("API escuchando en el puerto BIP BIP: " + port);




app.get ('/apitechu/v1/users', userController.getUsersV1);
app.get ('/apitechu/v2/users', userController.getUsersV2);
app.get ('/apitechu/v2/users/:id', userController.getUsersByIdV2);




app.get ('/apitechu/v2/account/:id', accountController.getAccountByUserIdV1);
app.get ('/apitechu/v2/accounts', accountController.getAccountsV1);


app.post('/apitechu/v1/users', userController.createUsersV1);
app.post('/apitechu/v2/users', userController.createUsersV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUsersV1);




//Practica LogIn/LogOut
app.post('/apitechu/v1/login', authController.createLoginV1);
app.post('/apitechu/v2/login', authController.createLoginV2);
app.post('/apitechu/v1/logout/:id', authController.createLogOutV1);
app.post('/apitechu/v2/logout/:id', authController.createLogOutV2);



//






app.get ('/apitechu/v1/hello',function(req,res) {
  console.log("GET /apitechu/v1/hello");
  res.send({"msg":"Hola desde API TechU"})
  }
)


// app.post('/apitechu/v1/monstruo/:p1/:p2',
// function(req,res){
//   console.log("Parametros");
//   console.log(req.params);
//
//   console.log("Query String");
//   console.log(req.query);
//
//   console.log("headers");
//   console.log(req.headers);
//
//   console.log("Body");
//   console.log(req.body);
//   }
// )

/*app.delete('/apitechu/v1/users/:id',
function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("La id ha eliminar es:" +req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    console.log("usuario borrado");
    writeUserDataToFile(users);


  }
)*/
