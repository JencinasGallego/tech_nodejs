const mocha = require('mocha'); // framework base de test unitarios para js
const chai =require('chai'); // Nos permite asserts más potentes
const chaihttp =require('chai-http'); //Nos pertime asserts en http

chai.use(chaihttp);

var should = chai.should(); //Un assert es un mecanismo para probar un invariante

describe('First test',
  function(){
    it('Test that DuckDuckgo works', function(done){
      chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function(err, res){
            // console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            done(); // las funciones manejadoras se encargan de la asincronia del codigo. done() comprueba las aserciones
          }
        )
      }
    )
  }
)

// describe es la suit de test
describe('Test de API usuarios',
  function(){
    it('Prueba que la API de usuarios responde correctamente', function(done){
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res){
            // console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU")
            done(); // las funciones manejadoras se encargan de la asincronia del codigo. done() comprueba las aserciones
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function(done){
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res){
            // console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            res.body.users.should.be.a('array'); // a hace referencia a metodos primitvos de js

            for(user of res.body.users){
              user.should.have.property('email');
              user.should.have.property('first_name');
            }





            done();
          }
        )
      }
    )
  }
)
