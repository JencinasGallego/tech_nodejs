const io = require('../io');

function loginV1(req, res) {

 console.log("POST /apitechu/v1/login");  var users = require('../usuarios.json');

 for (user of users) {

   if (user.email == req.body.email

       && user.password == req.body.password) {

     console.log("Email found, password ok");

     var loggedUserId = user.id;

     user.logged = true;

     console.log("Logged in user with id " + user.id);

     io.writeUserDataToFile(users);

     break;

   }

 }  var msg = loggedUserId ?

   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";  var response = {

     "mensaje": msg,

     "idUsuario": loggedUserId

 };  res.send(response);

}

function logoutV1(req, res) {

 console.log("POST /apitechu/v1/logout");  var users = require('../usuarios.json');

 for (user of users) {

   if (user.id == req.params.id && user.logged === true) {

     console.log("User found, logging out");

     delete user.logged

     console.log("Logged out user with id " + user.id);

     var loggedoutUserId = user.id;

     io.writeUserDataToFile(users);

     break;

   }

 }  var msg = loggedoutUserId ?

   "Logout correcto" : "Logout incorrecto";  var response = {

     "mensaje": msg,

     "idUsuario": loggedoutUserId

 };  res.send(response);

}module.exports.loginV1 = loginV1;

module.exports.logoutV1 = logoutV1;
