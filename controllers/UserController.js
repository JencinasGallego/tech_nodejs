require('dotenv').config();


const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req,res) {
  console.log('GET /apitechu/v1/users');

  // res.sendFile('usuarios.json', {root:__dirname});
  var users = require('../usuarios.json');// Nos mete la info de un archivo en una variable --../ no mueve a una ruta superior del path
 // res.send(users);
 //  Se definen las variables de los filtros
  var count = req.query.$count;
  var top = req.query.$top;
  var dataOut = {};
  if (!count&&!top)
  {
    console.log("Respuesta sin parametros");
    dataOut.users=users;

  }
  if (count&& count =="true"){
    console.log("recibido filtro count: " + count);
    dataOut.count=users.length;
    dataOut.users= users;
  }
  if(top && isNumeric(top)){
    if(top<=users.length){
      console.log("Recibido filtro top: " + top);
      dataOut.users= users.slice(0,top)
    }
    else{
      dataOut.errorMsg="El parametro es mayor al numero de elementos de la lista"
    }
  }
  res.send(dataOut)

}

function getUsersV2(req,res) {
  console.log('GET /apitechu/v2/users');
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}


function getUsersByIdV2(req,res) {
  console.log('GET /apitechu/v2/users/:id');



  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  var id = req.params.id;
  console.log('Id solicitado:' + id);
  var query ='q={"id":'+ id +'}';
  console.log(query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      if(err){
        var response = {
          "msg" : "Error obteniendo el usuario"
        }
        res.status(500);
      }else{
        if(body.length>0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Error obteniendo el usuario"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}



function createUsersV1(req, res){
    console.log("POST /apitechu/v1/users");
  //  console.log(req.headers);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUsers ={
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email":req.body.email
    }

    var users = require('../usuarios.json');
    users.push(newUsers); //Añade ususarios al final del array
    console.log("usuarios añadido");
    io.writeUserDataToFile(users);



  }

  function createUsersV2(req, res){
      console.log("POST /apitechu/v2/users");
    //  console.log(req.headers);
      console.log(req.body.id);
      console.log(req.body.first_name);
      console.log(req.body.last_name);
      console.log(req.body.email);
      console.log(req.body.password);

      var newUsers ={
        "id": req.body.id,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email":req.body.email,
        "password":crypt.hash(req.body.password)
      }

      console.log(newUsers);

      var httpClient = requestJson.createClient(baseMlabURL);
      console.log("client created");


    //  client.post('posts/', data, function(err, res, body)
      httpClient.post("user?" + mLabAPIKey, newUsers,
        function(err,resMLab,body){
          //console.log(resMLab);
          console.log("Usuario guardao con éxito");


          res.status(201).send({"msg":"Usuario guardao con éxito" });
        }
      );










      // var users = require('../usuarios.json');
      // users.push(newUsers); //Añade ususarios al final del array
      // console.log("usuarios añadido");
      // io.writeUserDataToFile(users);



    }


  function deleteUsersV1(req,res){
    console.log("DELETE practica /apitechu/v1/users/:id");
    console.log("El id recibido a borrar es: "+req.params.id)
    var users = require('../usuarios.json');

  // Recorrer array con for

  for (var i=0; i< users.length;i++){
    //  console.log(users[i].id);
    //  console.log("parametro: "+ req.params.id +"id: "+users[i].id);
      if (req.params.id == users[i].id){
        users.splice (i,1);
        console.log("Elemento borrado");
        break;

      }
    }
     io.writeUserDataToFile(users);


  // Recorrer array con for each -- no se puede usar break

    // users.forEach(function(user,index){
    //
    //     if (req.params.id == user.id){
    //       console.log("parametro: "+ req.params.id +" id: "+user.id);
    //       users.splice(index,1);
    //       console.log("Elemento borrado");
    //     }
    //   });
    //

  // Recorrer array con for..in --Mas recomendado para recorrer propiedades enumerables dentro de un objecto

  /* for(index in users){
      if (req.params.id == users[index].id){
        console.log(users[index]);
        users.splice(index,1);

        console.log("Elemento borrado");
        break;
      }


    }*/

  // Recorrer array con for..of --ojo si nos se devuleve el par clave - valor no conocemos la posición del objeto dentro del array



  // for (user of users){
  //   console.log(user);
  //   if (req.params.id == user.id){
  //     users.splice(user.id,1);
  //
  //     console.log("Elemento borrado");
  //
  //   }
  // }

}


module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;

module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUsersV1 = deleteUsersV1;
