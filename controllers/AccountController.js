require('dotenv').config();


const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;






function getAccountByUserIdV1(req,res) {
  console.log('GET /apitechu/v1/accounts/:id');



  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  var userId = req.params.id;
  console.log('UserId solicitado:' + userId);
  var query ='q={"UserID":'+ userId +'}';
  console.log(query);

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      if(err){
        var response = {
          "msg" : "Error obteniendo el usuario"
        }
        res.status(500);
      }else{
        if(body.length>0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Error obteniendo el usuario"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

function getAccountsV1(req,res) {
  console.log('GET /apitechu/v1/accounts');
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  httpClient.get("account?" +"&"+ mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);
      //console.log("body: "+body);
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}



















module.exports.getAccountByUserIdV1 = getAccountByUserIdV1;
module.exports.getAccountsV1=getAccountsV1
