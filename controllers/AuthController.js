const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechujeg9ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;


function createLoginV1(req,res){
  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var dataOut ={};
  var idOut ='';
  var userFinded= false;


  for (var [index, user] of users.entries()) {

    console.log("comparando " + user.email + " y " +  req.body.email);
    console.log("comparando " + user.password + " y " +  req.body.password);

    if (user.email == req.body.email && user.password == req.body.password) {
      console.log("Usuario encontrado");
      user.logged = true;
      idOut=user.id;
      userFinded= true;

      console.log(user);
      break;

    }


  }
  console.log("Ususario encontrado?: "+ userFinded);
  if (userFinded){

      io.writeUserDataToFile(users);
      dataOut.msg= "LogIn correcto";
      dataOut.id = idOut;

  }else{
      dataOut.msg= "LogIn incorrecto";

  }

   res.send(dataOut);
}

function createLoginV2(req,res){
  console.log("POST /apitechu/v2/login");
  console.log("Email: " + req.body.email);
  console.log("Password:" + req.body.password);
  var response={};
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  var email = req.body.email;
  var pass = req.body.password;

  var query ='q={"email":"'+ email +'"}';

  console.log(query);




  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLab,body){
      //console.log(resMLab);

      console.log(body);
      if(err){
        res.status(500);
        response.msg ="Login Incorrecto";
        console.log(response);
        res.send(response);
        res.status(500);
      }else{
        if(body.length>0){
          var passhash = body[0].password;
          console.log(passhash);
          console.log("Ckeck pass: "+pass+"||"+passhash);
          var found = crypt.checkPassword(pass,passhash);
          console.log("¿Usuario encontrado?: "+found);




        }else{
          var response = {
            "msg" : "Error obteniendo el usuario"
          }
          var found = false;
          res.status(401);
        }
	  }
    if(found){
      var putbody ='{"$set":{"logged":true}}';
      httpClient.put(
            "user?"
            + query
            + "&"
            + mLabAPIKey, JSON.parse(putbody), function(errPUT, resMLabPUT, bodyPUT){

              console.log(bodyPUT);

              if(err){

                res.status(500);
                response.msg ="Login Incorrecto";
                console.log(response);
                res.send(response);
              }else{
                var response={};
                res.status(200);
                response.msg ="Login correcto";
                response.id = body[0].id;
                console.log(response);
                res.send(response);


            }

          }
        )

      }else{
        var response={};
        res.status(401);
        response.msg ="Login Incorrecto";
        console.log(response);
        res.send(response);
      }




  }

 )



};






function createLogOutV1(req,res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log(req.params.id);


  var users = require('../usuarios.json');
  var userLogOut = false;
  var dataOut ={};
  var idOut='';

  // var userFinded= false;
  //
  //
  for (var [index, user] of users.entries()) {

    console.log("comparando " + user.id + " y " +  req.params.id);


    if (user.id == req.params.id && user.logged === true) {
      console.log("Usuario listo para logout");
      delete user.logged

      userLogOut = true;
      idOut=user.id;
      console.log(user);
      break;

    }


  }
  console.log("Ususario para hacer logOut?: "+ userLogOut);
  if (userLogOut){

      io.writeUserDataToFile(users);
      dataOut.msg= "Logout correcto";
      dataOut.id = idOut;

  }else{
      dataOut.msg= "Logout incorrecto";

  }

   res.send(dataOut);
}


function createLogOutV2(req,res){
  console.log("POST /apitechu/v2/logout/:id");
  console.log(req.params.id);
  var id = req.params.id;

  var query ='q={"id":'+ id +'}';

  var response={};
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab,body){
    //console.log(resMLab);

    console.log(body);
    if(err){
      var response = {
        "msg" : "Error 500 obteniendo el usuario"
      }
      res.status(500);
    }else{
      if(body.length>0){
        var logged = body[0].logged;





      }else{
        var response = {
          "msg" : "Error obteniendo el usuario"
        }
        var found = false;
        res.status(401);
      }
  }
      if(logged){
        var putbody='{"$unset":{"logged":""}}';
        httpClient.put(
          "user?"
          + query
          + "&"
          + mLabAPIKey, JSON.parse(putbody), function(errPUT, resMLabPUT, bodyPUT){

            console.log(bodyPUT);

            if(err){
              var response = {
                "msg" : "Error 500 obteniendo el usuario PUT"
              }
              res.status(500);
            }else{
              var response={};
              res.status(200);
              response.msg ="LogOut correcto";
              response.id = id;

      console.log(response);
      res.send(response);

          }

        }
      )

    }else{
      res.status(400);
      response.msg ="El logOut no se puede realizar";
      response.id = id;

console.log(response);
res.send(response);
    }




}

)




}

module.exports.createLoginV1=createLoginV1;
module.exports.createLoginV2=createLoginV2;
module.exports.createLogOutV1=createLogOutV1;
module.exports.createLogOutV2=createLogOutV2;
